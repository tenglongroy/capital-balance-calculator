# Capital Balance Calculator

## Introduction

An Angular powered Capital Balance Projection. There are several inputs that are transformed using calculations and then displayed on a graph. The calculator will be a front-end only tool.

The tool already has existing input values. Changing any value will trigger changes to the graph. Clicking "Open Data Table" at the bottom will popup the calculated data for reference.

This Angular project uses Angular 10, RxJs, NgRx, ng2-charts (an npm library for Chart.js) for implementation.

## Development server

Run `npm install` to start.
Then run `npm start` or `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
