import { Injectable } from "@angular/core";
import { RippleGlobalOptions } from "@angular/material/core";
import { IChartData, IControlValue } from "../models";

export const globalRippleConfig: RippleGlobalOptions = {
    animation: {
        enterDuration: 300,
        exitDuration: 500
    },
    terminateOnPointerUp: false
};

@Injectable({
    providedIn: 'root'
})
export class StaticMethodService {
    constructor(private staticData: StaticDataService) {}

    // generate xAxis labels starting from current year to 50 years later
    generateLabels(): string[] {
        const currentYear = this.staticData.currentYear;
        const result: string[] = []
        for(let year = currentYear; year <= currentYear+50; year++){
            result.push(year.toString());
        }
        return result;
    }

    // calculate chart series data following the formula in Excel spreadsheet, taking into account the contribution end year and withdraw start year
    generateSeriesData(controlValue: IControlValue): IChartData {
        const result = {
            startBalance: [],
            contributions: [],
            earnings: [],
            fees: [],
            tax: [],
            withdrawals: [],
            endBalance: []
        };
        const contributionInd = controlValue.contributeAge - this.staticData.currentYear;
        const withdrawInd = controlValue.withdrawAge - this.staticData.currentYear
        for(let ind = 0; ind <controlValue.seriesLength; ind++){
            if (ind == 0){
                result.startBalance[0] = controlValue.startBalance;
                result.contributions[0] = controlValue.salary * controlValue.contributionRate;
            }
            else{
                result.startBalance[ind] = result.endBalance[ind - 1];
                result.contributions[ind] = ind > contributionInd ? 0 : result.contributions[ind - 1] * (1 + controlValue.inflationRate);
            }
            result.earnings[ind] = (result.startBalance[ind] + result.contributions[ind]) * controlValue.earnings;
            result.fees[ind] = (result.startBalance[ind] + result.contributions[ind] + result.earnings[ind]) * controlValue.fees;
            result.tax[ind] = (result.contributions[ind] + result.earnings[ind]) * controlValue.tax;
            result.withdrawals[ind] = ind > withdrawInd ? result.startBalance[ind] * controlValue.withdrawalRate : 0;
            result.endBalance[ind] = result.startBalance[ind] + result.contributions[ind] + result.earnings[ind] - result.fees[ind] - result.tax[ind] - result.withdrawals[ind];
        }
        Object.keys(result).forEach(key => {
            for(let ind = 0; ind < result[key].length; ind ++){
                result[key][ind] = Number((result[key][ind]).toFixed(2));
            }
        })
        return result;
    }
}

@Injectable({
    providedIn: 'root'
})
export class StaticDataService {
    currentYear: number = new Date().getFullYear();

    constructor() {}

    // slider configuration
    ageContributeConfig: any = {
        connect: 'lower',
        start: this.currentYear + 20,
        range: {
            min: [this.currentYear, 1],
            max: this.currentYear + 50
        },
        tooltips: false
    };
    ageWithdrawConfig: any = {
        connect: 'upper',
        start: this.currentYear + 20,
        range: {
            min: [this.currentYear, 1],
            max: this.currentYear + 50
        },
        tooltips: false
    };
}