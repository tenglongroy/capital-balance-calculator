import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { MaterialModule } from './material.module';
import { NouisliderModule } from 'ng2-nouislider';
import { ChartsModule } from 'ng2-charts';
import { AutoSizeInputModule } from 'ngx-autosize-input';
import { CustomFontAwesomeModule } from './custom-font-awesome.module';
import { MAT_RIPPLE_GLOBAL_OPTIONS } from '@angular/material/core';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { globalRippleConfig } from './common-utilities';


@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        RouterModule,
        MaterialModule,
        CustomFontAwesomeModule,
        NouisliderModule,
        ChartsModule,
        AutoSizeInputModule
    ],
    exports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        MaterialModule,
        CustomFontAwesomeModule,
        NouisliderModule,
        ChartsModule,
        AutoSizeInputModule
    ],
    declarations: [
    ],
    entryComponents: [],
    providers: [
        { provide: MAT_RIPPLE_GLOBAL_OPTIONS, useValue: globalRippleConfig },
        {
            provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
            useValue: { appearance: 'fill' }
        }

    ]
})
export class SharedModule { }
