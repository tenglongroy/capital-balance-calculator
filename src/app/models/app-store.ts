import { IApp } from '.';
import { IChartData } from './chart-data';

export interface AppState {
    app: IApp;
    chartData: IChartData;
}
