

export interface IChartData{
    xAxis?: string[];
    startBalance?: number[];
    contributions?: number[];
    earnings?: number[];
    fees?: number[];
    tax?: number[];
    withdrawals?: number[];
    endBalance?: number[];
}