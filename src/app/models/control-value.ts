

export interface IControlValue{
    startBalance: number;
    salary: number;
    contributionRate: number;
    inflationRate: number;
    earnings: number;
    fees: number;
    tax: number;
    withdrawalRate: number;
    contributeAge: number;
    withdrawAge: number;
    seriesLength: number;
}