import * as chartDataActions from '../actions/chart-data.actions';
const cloneDeep = require('lodash/cloneDeep');
const deepMerge = require('lodash/merge');

export function chartDataReducer(state = null, action: chartDataActions.Action) {
    switch (action.type) {
        case chartDataActions.CREATE_LABELS_SUCCESS: {
            if (state == null){
                return {xAxis: action.payload};
            }
            else{
                const newState = cloneDeep(state);
                newState.xAxis = action.payload;
                return newState;
            }
        }
        case chartDataActions.CREATE_LABELS_FAIL: {
            return state;
        }
        case chartDataActions.GENERATE_SERIES_SUCCESS: {
            if (state == null){
                return action.payload;
            }
            else{
                const newState = cloneDeep(state);
                Object.assign(newState, action.payload);
                return newState;
            }
        }
        case chartDataActions.GENERATE_SERIES_FAIL: {
            return state;
        }
        default: {
            return state;
        }
    }
}
