import { IControlValue } from "../models";

export const CREATE_LABELS = 'CREATE_LABELS';
export const CREATE_LABELS_SUCCESS = 'CREATE_LABELS_SUCCESS';
export const CREATE_LABELS_FAIL = 'CREATE_LABELS_FAIL';

export const GENERATE_SERIES = 'GENERATE_SERIES';
export const GENERATE_SERIES_SUCCESS = 'GENERATE_SERIES_SUCCESS';
export const GENERATE_SERIES_FAIL = 'GENERATE_SERIES_FAIL';


// effect
export class CreateLabelsAction {
    readonly type = CREATE_LABELS;
    constructor(){}
}
export class CreateLabelsSuccessAction {
    readonly type = CREATE_LABELS_SUCCESS;
    constructor(public payload: any){}
}
export class CreateLabelsFailAction {
    readonly type = CREATE_LABELS_FAIL;
    constructor(){}
}

// effect
export class GenerateSeriesAction {
    readonly type = GENERATE_SERIES;
    constructor(public payload: IControlValue){}
}
export class GenerateSeriesSuccessAction {
    readonly type = GENERATE_SERIES_SUCCESS;
    constructor(public payload: any){}
}
export class GenerateSeriesFailAction {
    readonly type = GENERATE_SERIES_FAIL;
    constructor(){}
}


export type Action
  = CreateLabelsAction
  | CreateLabelsSuccessAction
  | CreateLabelsFailAction
  | GenerateSeriesAction
  | GenerateSeriesSuccessAction
  | GenerateSeriesFailAction;
