import { Store } from '@ngrx/store';
import { Inject, Injectable } from '@angular/core';
import { Effect, createEffect, Actions, ofType } from '@ngrx/effects';
import { of, from, Subscription, EMPTY, forkJoin } from 'rxjs';
import { catchError , switchMap, map, mergeMap } from 'rxjs/operators';

import * as appActions from '../actions/app.actions';
import * as chartDataActions from '../actions/chart-data.actions';

import { AppState } from '../models';
import { ActivatedRoute, Router } from '@angular/router';
import { DOCUMENT } from '@angular/common';
import { StaticMethodService } from '../shared/common-utilities';

const jwtDecode = require('jwt-decode');
const cloneDeep = require('lodash/cloneDeep');
const deepMerge = require('lodash/merge');


@Injectable()
export class GeneralEffects {

    private queryParams: any;
    private queryParamsSubscription: Subscription;

    constructor(private store: Store<AppState>,
                private activatedRoute: ActivatedRoute,
                private staticMethod: StaticMethodService,
                private window: Window,
                @Inject(DOCUMENT) private document: any,
                private actions$: Actions,
                private router: Router) {
        this.queryParamsSubscription = this.activatedRoute.queryParams.subscribe(value => {
            this.queryParams = value;
        });
    }

    

    createLabels$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(chartDataActions.CREATE_LABELS),
            switchMap((action: chartDataActions.CreateLabelsAction) => {
                try{
                    const labels = this.staticMethod.generateLabels();
                    return of(new chartDataActions.CreateLabelsSuccessAction(labels));
                }
                catch(error){
                    return of(new chartDataActions.CreateLabelsFailAction());
                }
            })
        );
    });

    generateSeries$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(chartDataActions.GENERATE_SERIES),
            switchMap((action: chartDataActions.GenerateSeriesAction) => {
                try{
                    const chartData = this.staticMethod.generateSeriesData(action.payload);
                    return of(new chartDataActions.GenerateSeriesSuccessAction(chartData));
                }
                catch(error){
                    return of(new chartDataActions.GenerateSeriesFailAction());
                }
            })
        );
    });
}