import { Component, OnDestroy, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { FormControl, FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { AppState, IControlValue } from './models';
import * as chartDataActions from './actions/chart-data.actions';
import { StaticDataService } from './shared/common-utilities';
import { Label } from 'ng2-charts';
import { debounce, debounceTime, filter } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { PopupTableComponent } from './components/popup-table.component';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy{
    title = 'capital-balance-calculator';

    lineChartLabels: Label[] = [];
    hasInitChart: boolean = false;
    formulaInputGroup: FormGroup;
    private subformulaInputGroup: Subscription;
    private subControlValue: Subscription;


    constructor(private store: Store<AppState>,
                private dialog: MatDialog,
                public staticData: StaticDataService){}

    ngOnInit(){
        this.formulaInputGroup = new FormGroup({
            startBalance: new FormControl(300000, Validators.required),
            salary: new FormControl(100000, [Validators.required, Validators.min(0)]),
            contributionRate: new FormControl(9.5, [Validators.required, Validators.min(0)]),
            inflationRate: new FormControl(3, Validators.required),
            earnings: new FormControl(7.5, [Validators.required, Validators.min(0)]),
            fees: new FormControl(1.5, [Validators.required, Validators.min(0)]),
            tax: new FormControl(15, [Validators.required, Validators.min(0)]),
            withdrawalRate: new FormControl(5, [Validators.required, Validators.min(0)]),
            ageContributeSlider: new FormControl(this.staticData.currentYear + 20, Validators.required),
            ageWithdrawSlider: new FormControl(this.staticData.currentYear + 20, Validators.required),
        });

        // get xAxis label array so that length is decided before generating series data from controlValue
        this.subControlValue = this.store.select(state => state.chartData).pipe(filter(data => !!data)).subscribe(chartData => {
            this.lineChartLabels = chartData.xAxis;
            if (!this.hasInitChart && chartData.xAxis?.length && this.formulaInputGroup.valid){
                const controlValue: IControlValue = {
                    seriesLength: this.lineChartLabels.length,
                    startBalance: this.formulaInputGroup.value.startBalance,
                    salary: this.formulaInputGroup.value.salary,
                    contributionRate: this.formulaInputGroup.value.contributionRate/100,
                    inflationRate: this.formulaInputGroup.value.inflationRate/100,
                    earnings: this.formulaInputGroup.value.earnings/100,
                    fees: this.formulaInputGroup.value.fees/100,
                    tax: this.formulaInputGroup.value.tax/100,
                    withdrawalRate: this.formulaInputGroup.value.withdrawalRate/100,
                    contributeAge: this.formulaInputGroup.value.ageContributeSlider,
                    withdrawAge: this.formulaInputGroup.value.ageWithdrawSlider
                };
                this.store.dispatch(new chartDataActions.GenerateSeriesAction(controlValue));
                this.hasInitChart = true;
            }
        });

        // debounce 500 ms to avoid too frequently updating the chart
        this.subformulaInputGroup = this.formulaInputGroup.valueChanges.pipe(debounceTime(500)).subscribe(value => {
            if(this.formulaInputGroup.valid){
                const controlValue: IControlValue = {
                    seriesLength: this.lineChartLabels.length,
                    startBalance: value.startBalance,
                    salary: value.salary,
                    contributionRate: value.contributionRate/100,
                    inflationRate: value.inflationRate/100,
                    earnings: value.earnings/100,
                    fees: value.fees/100,
                    tax: value.tax/100,
                    withdrawalRate: value.withdrawalRate/100,
                    contributeAge: value.ageContributeSlider,
                    withdrawAge: value.ageWithdrawSlider
                };
                this.store.dispatch(new chartDataActions.GenerateSeriesAction(controlValue));
            }
        });

        this.store.dispatch(new chartDataActions.CreateLabelsAction());
    }

    ngOnDestroy(){
        !!this.subformulaInputGroup && this.subformulaInputGroup.unsubscribe();
        !!this.subControlValue && this.subControlValue.unsubscribe();
    }

    openDialog() {
        const dialogRef = this.dialog.open(PopupTableComponent, {
            width: '90%',
        });
    }
}
