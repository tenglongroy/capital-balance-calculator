import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from 'src/environments/environment';
import { GeneralEffects } from './effects/general.effects';
import { appReducer } from './reducers/app.reducer';
import { chartDataReducer } from './reducers/chart-data.reducer';
import { SharedModule } from './shared/shared.module';
import { ChartComponent } from './components/chart.component';
import { PopupTableComponent } from './components/popup-table.component';


const imports: any[] = [
    SharedModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    StoreModule.forRoot({
        app: appReducer,
        chartData: chartDataReducer,
    }),
    EffectsModule.forRoot([GeneralEffects]),
];

const devImports: any[] = [
    // NOTE: instrument must come *after* importing StoreModule
    StoreDevtoolsModule.instrument()
];

if (!environment.production) {
    imports.push(devImports);
}


@NgModule({
    declarations: [
        AppComponent,
        ChartComponent,
        PopupTableComponent
    ],
    imports: [
        imports
    ],
    providers: [
        { provide: Window, useValue: window },
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
