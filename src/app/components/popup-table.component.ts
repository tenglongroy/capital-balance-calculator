import { Component, OnDestroy, OnInit } from "@angular/core";
import { Store } from "@ngrx/store";
import { Subscription } from "rxjs";
import { filter } from "rxjs/operators";
import { AppState, IChartData } from "../models";

@Component({
    selector: 'popup-table-component',
    templateUrl: 'popup-table-component.html',
  })
export class PopupTableComponent implements OnInit, OnDestroy{

    chartData: IChartData;
    private subChartData: Subscription;
    constructor(private store: Store<AppState>){}

    ngOnInit(){
        // get data for display
        this.subChartData = this.store.select(store => store.chartData).pipe(filter(chartData => !!chartData)).subscribe(chartData => {
            this.chartData = chartData;
        });
    }
    ngOnDestroy(){
        !!this.subChartData && this.subChartData.unsubscribe();
    }
}