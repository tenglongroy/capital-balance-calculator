import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, BaseChartDirective, Label } from 'ng2-charts';
import * as pluginAnnotations from 'chartjs-plugin-annotation';
import { StaticMethodService } from '../shared/common-utilities';
import { Store } from '@ngrx/store';
import { AppState } from '../models';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';


/* https://valor-software.com/ng2-charts/#/LineChart */
@Component({
    selector: 'app-chart',
    templateUrl: './chart.component.html'
})
export class ChartComponent implements OnInit, OnDestroy {
    lineChartData: ChartDataSets[] = [
        { data: [], label: 'Start Balance' }
    ];
    lineChartLabels: Label[] = [];
    lineChartOptions: ChartOptions = {
        responsive: true,
        maintainAspectRatio: false,
        legend: {
            display: false
        },
        scales: {
            xAxes: [{}],
            yAxes: [
                {
                    id: 'y-axis-0',
                    position: 'left',
                }
            ]
        }
    };
    lineChartColors: Color[] = [
        { // grey
            backgroundColor: 'rgba(148,159,177,0.2)',
            borderColor: 'rgba(148,159,177,1)',
            pointBackgroundColor: 'rgba(148,159,177,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(148,159,177,0.8)'
        }
    ];
    lineChartLegend: boolean = true;
    lineChartType: ChartType = 'line';
    lineChartPlugins = [pluginAnnotations];

    @ViewChild(BaseChartDirective, { static: true }) chart: BaseChartDirective;
    @Input('startBalance') shareGroup: any;

    private subControlValue: Subscription;

    constructor(private store: Store<AppState>,
                private staticMethod: StaticMethodService) { }

    ngOnInit(): void {
        // get data for display
        this.subControlValue = this.store.select(state => state.chartData).pipe(filter(data => !!data)).subscribe(chartData => {
            this.lineChartLabels = chartData.xAxis;
            this.lineChartData[0].data = chartData.startBalance;
        });
    }
    ngOnDestroy(): void {
        !!this.subControlValue && this.subControlValue.unsubscribe();
    }
}